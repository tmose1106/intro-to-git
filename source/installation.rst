************
Installation
************

Before we can begin using Git, we must install it.

Windows
=======

* Visit the `Git for Windows`_ website
* Click the big blue "Download" button to be redirected to GitHub
* Download the most recent 64-bit Git exe file (i.e. *Git-2.21.0-64-bit.exe*)
* Follow the installation wizard

.. _`Git for Windows`: https://gitforwindows.org/

macOS
=====

.. note::

   If you have installed Xcode before, there is a chance that Git may
   already be installed. We will check our installation in the next section.

* Visit the `git-osx-installer`_ page on SourceForge
* Click the big green "Download" button

.. _`git-osx-installer`: https://sourceforge.net/projects/git-osx-installer/

GNU/Linux
=========

Depending upon your distribution, the installation instructions will differ.
Pretty much every distribution has Git available in its packaging repository.
Read the `official download page`_ for more information.

.. _`official download page`: https://git-scm.com/download/linux
