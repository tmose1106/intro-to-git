************
Introduction
************

The purpose of this tutorial is to teach basic command line usage of the `Git`_
`version control system`_.

.. _`Git`: https://en.wikipedia.org/wiki/Git
.. _`version control system`: https://en.wikipedia.org/wiki/Version_control

Why?
====

Along with writing code comes the problem of managing the corresponding
source files. Managing these files by hand is error-prone and time
consuming. This is where a VCS comes in: a program designed to manage
source files *effectively*.

Today, Git is the most popular version control system. The `2018 Stack
Overflow Developer Survey`_ recorded that about **87% of developers** use Git
as their main version control system.

Throughout this tutorial, you may be asking "Why don't I just use a Git GUI?"
and the answer is that Git's command line interface is almost always available
regardless of platform and once you learn to use it on one system, you can
use it on essentially any system.

.. _`2018 Stack Overflow Developer Survey`: https://insights.stackoverflow.com/survey/2018/#work-version-control

Historical Background
=====================

Throughout the years there have been many attempts at solving this
problem. Some notable projects include `Concurrent Versions System (CVS)`_,
`Apache Subversion (SVN)`_ and `Bitkeeper`_. Though each of these solutions
has seen commercial usage in the past, they each had their own shortcomings.

Disappointed with every solution available at the time, `Linux kernel`_
creator `Linus Torvalds`_ set out to create a light-weight, distributed and
safe version control system which could handle small and large projects alike.
He began work in April of 2005 and by the mid-June his solution was put into
production. This solution was Git. Since then, Git has taken a lion's share
of VCS marketshare.

.. _`Apache Subversion (SVN)`: https://en.wikipedia.org/wiki/Apache_Subversion
.. _`Bitkeeper`: https://en.wikipedia.org/wiki/BitKeeper
.. _`Concurrent Versions System (CVS)`: https://en.wikipedia.org/wiki/Concurrent_Versions_System
.. _`Linus Torvalds`: https://en.wikipedia.org/wiki/Linus_Torvalds
.. _`Linux kernel`: https://en.wikipedia.org/wiki/Linux
