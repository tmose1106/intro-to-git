**************
Basic Commands
**************

.. role:: yellow
.. role:: red
.. role:: green

Now that we have a baseline for command line usage and how Git works,
we can begin using Git.

Subcommands
===========

The Git command line interface makes use of subcommands.

Now, let's discuss a subset of the available subcommands:

init

  Command to initialize a local Git repository.

  In reality this creates a folder name *".git"* which stores all the data
  pertaining to the repository. This is where configuration is stored,
  the data for the staging index and commits, etc. The contents of this folder
  is also what is sent to remote repositories when you push data. So, it is of
  utmost importance that you do not delete this folder while working.

status

  Show the current status of the repository.

  This will show information about tracked files (untracked, changed, removed,
  added) as well as other information.

config

  Set repository and global configuration values.

  There are two ways to set configuration values: with and without the
  ``--global`` configuration flag. This will determine whether the
  value is set for the local repository, or for the system you are
  currently using.

  Some useful option flags are:

  --get

    Get a current configuration value

  --get-all

    Get a list of all configuration values

  --global

    Set a value globally (for all future repositories). The default is to
    set only for the current repository.

add *[PATH...]*

  Add a file's contents to the staging index. The *PATH* arguments are file
  paths to be added.

  Some useful option flags are:

  -A, --all

    Add all untracked and dirty files to the working index.

mv

  Move or rename a file or directory. This will rename the file in the working
  directory as well as in the staging index.

rm

  Remove files from the index.

  Some useful option flags are:

  -f, --force

    Remove the file/directory in the working directory

  -r

    Recursively remove files from a directory

commit

  Record a set of changes from the index.

  Some useful flags are:

  -m, --message *MSG*

    The *MSG* argument should be a string containing the commit message. If
    this option is not passed, a text editor will be opened for writing a
    message.

push

  Update remote repositories with recent commits.

pull

  Copy changes from remote repository to an existing local repository.

clone *URL* *[NAME]*

  Copy a remote repository at *URL* to a local repository. If the *NAME*
  argument is passed, copy to directory of that name.

Usage Tutorial
==============

Now, let's see the above commands in action. First things first, we need to
configure Git with our user credentials. In order to use Git, we must give it
a name and an email.

.. container:: term

  ::

    user@localhost ~> git config --global user.name "Ted Moseley"
    user@localhost ~> git config --global user.email "tmose@gmail.com"

Since we used the ``--global`` flag, this configuration is a one-time setup.
In the future, you will only have to do this when using a new computer.

Next, we will create a directory called ``example-repo`` and initialize an
empty Git repository.

.. container:: term

  ::

    user@localhost ~> mkdir example-repo
    user@localhost ~> cd example-repo
    user@localhost ~/example-repo> git init
    Initialized empty Git repository in /home/tedm/example-repo/.git/

To check that the initialization worked, let's check our repository's status:

.. container:: term

  ::

    user@localhost ~> git status
    On branch master

    No commits yet

    nothing to commit (create/copy files and use "git add" to track)

It looks good. Next, we will create a file called *hello.py* with a
"Hello World" example inside. Let's put the following contents inside:

.. code-block:: python3

  print("Hello, World!")

.. container:: term

  ::

    user@localhost ~/example-repo> git status
    On branch master

    No commits yet

    Untracked files:
      (use "git add <file>..." to include in what will be committed)

    hello.py

    nothing added to commit but untracked files present (use "git add" to track)

Now we will add the file to the index and recheck our repo's status.

.. container:: term

  ::

    user@localhost ~/example-repo> git add hello.py
    user@localhost ~/example-repo> git status
    On branch master

    No commits yet

    Changes to be committed:
      (use "git rm --cached <file>..." to unstage)

    	new file:   hello.py

Our file's current state was successfully added. Let's add more to *hello.py*.

.. code-block:: python3

  print("Hello, World!")
  print("How are you?")

Now, if we check back on our repo's status...

.. container:: term

  ::

    user@localhost ~/example-repo> git status
    On branch master

    No commits yet

    Changes to be committed:
      (use "git rm --cached <file>..." to unstage)

    	new file:   hello.py

    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)

    	modified:   hello.py

It notices that there has been changes since we initially added the file. If
we would like to update the file's state in the index we must add it again.
You will typically want to add files when you are done editing them, not
between every edit.

Let's create a README file called *README.md*:

.. code-block:: md

  # example-repo

  An example repository

Let's add these files and commit our changes.

.. container:: term

  ::

    user@localhost ~/example-repo> git add --all
    On branch master

    No commits yet

    Changes to be committed:
      (use "git rm --cached <file>..." to unstage)

    	new file:   README.md
    	new file:   hello.py

    user@localhost ~/example-repo> git commit -m "Here's a commit message"

We have made the commit. The next step is to push the changes to a
remote server. Our blank repository does not have any remotes set, so we
must set one. In the next section we will discuss remotes further. For the
time being, just take it that the ``git remote`` command adds a remote.
We will also set our upstream URL to our new remote so that whenever we push
for now on, we will only need to use ``git push``.

.. container:: term

  ::

    user@localhost ~/example-repo> git remote add origin https://github.com/tmose1106/example-repo.git
    user@localhost ~/example-repo> git push --set-upstream origin master

.. note::

  This part isn't explained very well since we will find a way around these
  boilerplate commands in the next section.

Here on out, we can skip the above steps and simply use the ``push``
subcommand by itself. When we do so, we will be asked to enter our sign
in credentials.

.. container:: term

  ::

    user@localhost ~/example-repo> git push
    Username for 'https://github.com': tmose1106
    Password for 'https://tmose1106@github.com':
    Enumerating objects: 5, done.
    Counting objects: 100% (5/5), done.
    Delta compression using up to 8 threads
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (3/3), 306 bytes | 306.00 KiB/s, done.
    Total 3 (delta 0), reused 0 (delta 0)
    To https://github.com/tmose1106/example-repo.git
       e6b58bd..cd76382  master -> master
