******************
Editor Integration
******************

Provide Git's popularity and large user base, it is no surprise that Git has
integration with many text editors and IDEs.

Atom
====

Recent versions of Atom come with integration for Git commands. This can be
accessed using *Package > GitHub > Toggle Git Tab*.

.. image:: _static/atom-open-git.png
  :align: center
  :scale: 60%

This will open a tab on the right side of the editor which shows staged files,
a box to enter a commit message and a list of past commits.

.. image:: _static/atom-git-menu.png
  :align: center
  :scale: 40%

Since Atom is developed by GitHub, it stands to reason they provide GitHub
integration. By opening *Package > GitHub > Toggle GitHub Tab*, another tab
will open on the right side of the screen allowing one to link with their
GitHub account and view pull requests.

.. image:: _static/atom-github-menu.png
  :align: center
  :scale: 40%

VS Code
=======

The VS Code documentation has a `fabulous article on Git integration`_. All
the information you need is there.

.. note::

  VS Code does not come with Git, so one must install it manually. Just follow
  the steps in `Installation`_ under Windows.

.. _`fabulous article on Git integration`: https://code.visualstudio.com/Docs/editor/versioncontrol
.. _`Installation`: ./installation.html

Xcode
=====

When Xcode is installed, it comes with a version of Git. Using this, Xcode
also has a GUI interface for performing Git commands. Watch `this video on
Git and Xcode 10`_ for more information.

GitHub also streamlines the process of opening projects by providing an
*Open in Xcode* option in the clone menu.

.. image:: _static/open-in-xcode.png
  :align: center
  :scale: 60%

.. _`this video on Git and Xcode 10`: https://www.youtube.com/watch?v=TlSxepPFIMs
