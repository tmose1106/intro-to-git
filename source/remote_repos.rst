*******************
Remote Repositories
*******************

One of the benefits of Git is that it is decentralized. By using a
decentralized VCS, one is able to access repositories remotely. One
can push a commit on one computer and then pull it down on another
using a single command. This is extremely powerful for cooperative
programming.

In order to ease the experience of working with remote repositories,
several projects have been created to build more features around Git.
One of these is the remote hosting website.

GitHub Overview
===============

Today, GitHub is currently the most popular repository hosting website. For
this reason, it is worthwhile for us to take a look at what GitHub has to
offer. Here are a few reasons why one may like to use a site like GitHub:

* Host unlimited public repositories for free
* Provide a neat visual interface for viewing projects
* Makes cloning repositories very simple
* Allow users to report bugs on a project
* Host static websites and wikis for free
* Adds a "social" aspect to programming

GitHub Usage Tutorial
=====================

Using GitHub is a relatively simple and straightforward process, but it is
worth highlighting how to use some of GitHub's features.

Upon visiting the GitHub homepage, there will be a convenient form for signing
up. Fill this out if you have to created an account before. Otherwise, log-in
to your account using the "Sign in" button up top.

.. image:: _static/github-signup.png
  :align: center
  :scale: 50%

Once you log in and verify yourself, you will be greeted with your overview
screen. On the left you will see your recently contributed repositories. The
center shows a feed of recent activity on repositories you are watching.

.. image:: _static/github-user-home.png
  :align: center
  :scale: 50%

Alternatively, you can access many parts of GitHub using the menu in the top
right corner by clicking on your profile picture.

.. image:: _static/github-user-menu.png
  :align: center

By clicking on "Your repositories" in this menu, we can see a list of projects
that we own, are associated with or have worked on.

.. image:: _static/github-user-repos.png
  :align: center
  :scale: 50%

By clicking the large green "new" button, we can begin creating a new
repository. This is a fairly streamlined process and only requires filling out
a few text boxes and checking a few settings.

.. image:: _static/github-repo-new.png
  :align: center
  :scale: 75%

Now we have access to the repository home page. Here we can see:

* A load of tabs and buttons
* A blue bar containing the most recent commit message and the commiter's name
  (from our ``git config`` earlier)
* A list of files and directories inside of our project
* A nice rendering of our `Markdown`_ formatted README file

All in all, this provides a concise overview of one's programming project.

.. image:: _static/github-repo-home.png
  :align: center
  :scale: 50%

For others to clone your repository, all they have to do is hit the green
"Clone or download" button and copy the URL. This URL can be passed to
``git clone`` as an argument, and Git will handle the rest.

.. image:: _static/github-repo-clone.png
  :align: center

If we click on the "n Commit" button, we can see a list of recent commits.

.. image:: _static/github-repo-commits.png
  :align: center
  :scale: 50%

By clicking on the "Issues" tab, we can see where users are allowed to file
bug reports or as they are called on GitHub, *issues*. Here you can respond
to users, decided who in your team will work on an issue, etc.

.. image:: _static/github-repo-issue.png
  :align: center
  :scale: 50%

By clicking on the the "Pull requests" tab, you can see a list of forks with
new contributions to be added to your project. We will discuss this more
later (after we talk about branching) but this is how you contribute to a
repository that is not yours.

.. image:: _static/github-repo-requests.png
  :align: center
  :scale: 50%

Finally, there are many more options for your repository under the "Settings"
tab.

.. image:: _static/github-repo-settings.png
  :align: center
  :scale: 50%

GitHub has many more features available that may be worthwhile to research.
However, these are the core features of GitHub.

.. _`Markdown`: https://commonmark.org/help/

Putting It Together
===================

Now we can use this in conjunction with our Git knowledge. If we want to pull
a copy of the repository from GitHub, we can simply use the ``clone``
subcommand.

.. container:: term

  ::

    user@localhost ~> git clone https://github.com/tmose1106/example-repo.git
    Cloning into 'example-repo'...
    remote: Enumerating objects: 3, done.
    remote: Counting objects: 100% (3/3), done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
    Unpacking objects: 100% (3/3), done.

Now the contents of the repository will be stored inside of a folder with the
same name as the repository. Also, the remote and upstream URL will be preset
for you.

Alternatives
============

There are a number of  hosting site alternative to GitHub. There are three
worth mentioning:

`cgit`_

  A barebones Git web frontend.

`Gitea`_

  An open source self-hosting repository server written in Go.

`Gitlab`_

  An open source, large scale hosting server which has a hosted version online
  (much like GitHub) or can be self-hosted. Can also be used at the commercial
  level.

`Gogs`_

  A painless self-hosted Git service.

.. _`cgit`: https://git.zx2c4.com/cgit/about/
.. _`Gogs`: https://gogs.io/
.. _`Gitlab`: https://about.gitlab.com/
.. _`Gitea`: https://gitea.io/en-us/
