*********************
Intermediate Commands
*********************

Cleaning the Working Directory
==============================

reset
  Reset unstaged changes

  This subcommand takes a *mode* flag which determines how it acts

  --hard
    Remove all changes from the index and working directory

  --merge
    Remove changes from index, but attempt to update commited files

  --soft
    Attempt to keep all changes but change name of current commit

clean
  Remove untracked changes

  This subcommand needs to take one of the following flags in order to make
  changes:

  -f, --force
    Confirm removal of all unstaged files

  -i, --interactive
    Prompt for removal of each individual untracked file

  -n, --dry-run
    Don't remove anything, only print what would be deleted

stash
  Save all uncommited changes, returning to the most recent commit's state

  Used for saving any changes you made before switching branches or pulling
  in new changes.

  In order to use the saved stash, there are a number of useful subcommands:

  list
    List all saved stashes

  apply
    Reapply the most recent stash

  pop
    Reapply the most recent stash **and** remove it from stash history

Working with Branches
=====================

By default, we work on the *master* branch of our repository. However,
this branch is typically reserved for working copies of your project which
can be accessed by anyone.

When we want to work on a new feature or are working with a group of
individuals, we utilize the concept of **branching**. Branches allow us
to create an alternate history of commits. Once a feature is complete, we
can **merge** it back into the master branch by adding the new commits
into the master branch's history.

branch *[NAME]*
  Manage branches in the repository

  Running the command without flags simply prints all of the available
  branches, with a ``*`` next to the active branch.

  If a name argument is passed, a branch with that name will be created.

  -a, --all
    List all branches (remote and local)

  -d, --delete
    Delete a local branch

  -m, --move
    Rename a branch

checkout
  Switch between branches

  .. note::

    Make sure that the working directory is clean. Any non-committed files
    in the current branch will be copied to the new branch. If you wish to
    switch branches without changes, use one of the method described above
    or the ``-f`` flag below.

  Some useful option flags are:

  -b *NAME*
    Create a branch with name *NAME* and change to it

  -f
    Remove all local changes when switching

merge *[BRANCH...]*
  Join branch histories together

  The branches in argument *BRANCH* will be merged into the current branch.

  .. note::

    Depending on circumstances, merging can be quite complex and troublesome.
    Let's say two users attempt edit the same file and they both wish to
    merge. This will produce a **merge conflict** which must be handles
    manually.

.. _`this StackOverflow question`: https://stackoverflow.com/questions/1304626/git-switch-branch-and-ignore-any-changes-without-committing

Reverting Commit History
========================

Occasionally, you may want to revert changes that were made to a branch.
For example, a major bug may have been commited to your *master* branch on
accident, so you want to go back to the commit where your project was working
last.

While we could do so using a variation of ``git reset``, that isn't technically
correct since this would remove history from our project. We want to keep a
record of what occured. For this reason, we will be using ``git revert``
instead.

We can use a command like shown below to revert the last three commits on the
branch:

::

  git revert HEAD~3

However, if you are working with many people and are reverting *master*, there
are likely to be merges in the history. In this case, git will not know which
branch to revert to and `complain about it`_. This can be fixed by passing the
``-m 1`` option.

::

  git revert -m 1 HEAD~3

By default, this will open an editor to enter a commit message. If this is not
wanted, simply pass the ``-n`` flag, short for ``--no-commit``. This will just
make the changes to the index and working directory so you can make your own
commit later.

.. _`complain about it`: https://stackoverflow.com/questions/5970889/why-does-git-revert-complain-about-a-missing-m-option
