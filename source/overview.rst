***************
Overview of Git
***************

We now know plenty of command line terminology, but before we start using Git
it is important to understand what Git is doing, and how it works at a higher
level.

General Workflow
================

While using Git, the workflow is as follows:

* Create text files (i.e. source code) using a text editor/IDE and storing
  them in a directory

* Add each file's current contents to a "staging area"

* Take a snapshot of the staging area

* Send that snapshot to a server where it can be accessed remotely

Terminology
===========

Repository

  A place where code is stored. In this case, commonly called a *Git
  repository* or *repo* for short.

Working Directory

  The directory (folder) which contains all of the project files.

Index

  Where the current contents of our files are stored. Sometimes known as the
  *staging area* since it is where files are prepared for a commit. When I
  file is added to the index, it is consider to be *tracked*.

  Files are added to the index manually using ``git add``.

Commit

  Essentially a snapshot of the index at a point in time. Commits also include
  information like when it was created, who created it and a message of why
  the commit itself was made.

  A commit is created using ``git commit``.

Remote

  A server where a repository and its commits are kept. Typically this server
  is not local, but remotely accessed over the internet.

Under the Hood
==============

How does Git do all of this behind the scenes? Without getting into many
details, Git stores all of its data inside of a directory named ".git". As
files are staged and commits are created, they are written into this
directory. When we are ready, we send the commits stored in this directory to
the remote (AKA *pushing*) to be stored remotely and shared.
