******************
Command Line Usage
******************

In order to use Git's command line interface, it is worthwhile for us to
become moderately familiar with the command line itself.

Terminology
===========

The command line is a relatively simple program which allows the
user to interact with their computer through a text-based interface.
Along with this comes a set of terminology. Below is a selected list
of important definitions along with highlighted examples.

Path

  A list of names separated by slashes defining the *location* of a file (*file
  path*) or "folder" (*directory path*) in the file-system

  .. container:: term

    ::

      user@localhost ~> cd Projects

Home Directory

  The home directory for the logged in user. Where folders like *Documents*,
  *Downloads*, *Music*, etc. are found. In the terminal, the tilda character
  (``~``) is replaced with the user's home directory.



Working Directory

  The directory that the command line is acting relative to. When a new
  command line is opened, the working directory will be your home directory.

Prompt

  A message output by the command line when ready for user input. Typically
  informs the user of who they are logged in as, which computer they are using
  and where they are in the file-system.

  .. container:: term

    ::

      user@localhost ~/Projects>

Command

  The name of a program typed on the command line to perform a specific task.

  .. container:: term

    ::

      user@localhost ~> cd

Argument

  Input for a command to act upon

  .. container:: term

    ::

      user@localhost ~> cd Projects

Subcommand

  A specially named argument which tells the command how to act.

  .. container:: term

    ::

      user@localhost ~/Project> git add src/main.cpp

Short Option Flag

  A special argument which changes how the command behaves. Typically denoted
  with a leading a ``-`` character

  .. container:: term

    ::

      user@localhost ~/Projects> git commit -m "Initial commit"

Long Option Flag

  A special argument which changes how the command behaves. Typically denoted
  with a leading ``--`` characters

  .. container:: term

    ::

      user@localhost ~/Projects> git add --force src/complex.cpp

Basic Commands
==============

Now that we know some basic terminology, it is good to learn a few basic
UNIX commands.

.. note::

  Powershell on Windows has commands of the same name, but they act somewhat
  differently. It is better to use the shell that comes with Git.

pwd

  Print the directory you are is inside of.

  .. container:: term

    ::

      user@localhost ~> pwd
      /home/user

cd *[ARG]*

  Change to a directory *ARG*. If no argument provided, change to the user's
  home directory.

  .. container:: term

    ::

      user@localhost ~> cd Documents

ls *[ARG]*

  List the files and directories inside of directory *ARG*. By default, *ARG*
  is equal to the directory you are inside of.

  .. container:: term

    ::

      tedm@tedm-arch ~/Documents> ls
      'Learning OpenCV 3.pdf'*
      passwords-20180830.kdbx*
      python-3.7.0a3-docs-html/
      resume_180514.odt
      resume_180514.pdf
      Textbooks/
      University/

mkdir *ARG*

  Create a directory with name *ARG*.

  .. container:: term

    ::

      user@localhost ~/Documents> mkdir Projects
      user@localhost ~/Documents> ls
      'Learning OpenCV 3.pdf'*
      passwords-20180830.kdbx*
      Projects/
      python-3.7.0a3-docs-html/
      resume_180514.odt
      resume_180514.pdf
      Textbooks/
      University/

