Welcome to Intro to Git!
========================

The purpose of this site is to provided intermediate understanding of Git's
command line interface to complete beginners through simple and concise
examples. This site also provides the very basics of general command line
usage as well as an overview of GitHub's web interface.

.. toctree::
  :maxdepth: 1
  :hidden:

  introduction
  installation
  command_line
  overview
  basic_commands
  intermediate_commands
  remote_repos
  editors

About This Workshop
===================

This site is being developed for the Rutgers University `Novice to Expert
Coding Club`_ (N2E) Git workshop. It is intended to act as "slides" to aid
in teaching Git at a one-time two hour lecture workshop hosted by N2E, as
well as a resource which can be directly used by students as a brief and
straight-forward reference.

============= =====================================
Live Website  `tmose1106.gitlab.io/intro-to-git`_
Source Code   `gitlab.com/tmose1106/intro-to-git`_
============= =====================================

.. _`Novice to Expert Coding Club`: http://n2ecodingclub.rutgers.edu/
.. _`tmose1106.gitlab.io/intro-to-git`: https://tmose1106.gitlab.io/intro-to-git/index.html
.. _`gitlab.com/tmose1106/intro-to-git`: https://gitlab.com/tmose1106/intro-to-git

Licensing
=========

This project is released under the terms of the `Creative Commons Attribution
4.0 International`_ license. For more information, read the *LICENSE.txt*.

.. _`Creative Commons Attribution 4.0 International`: http://creativecommons.org/licenses/by/4.0/
